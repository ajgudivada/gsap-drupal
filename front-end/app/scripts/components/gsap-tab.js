//function to handle tab click event
function switchTabs(event) {
  //get the number of tab pressed
  var txt = event.currentTarget.dataset.index;
  var num = txt.replace(/[^0-9]/g, '');

  var tabListItems = document.querySelectorAll('.tab-items>li>a');
  //toggle active class from header
  tabListItems.forEach(function (ele) {
      if (ele.dataset.index == 'tab-item-' + num) {
          ele.classList.add('active-tab');
      } else {
          ele.classList.remove('active-tab');
      }
  });

  var tabBodyElements = document.querySelectorAll('.tab-body-container>div');
  //toggle active class body
  tabBodyElements.forEach(function (ele) {
      if (ele.dataset.index == 'tab-body-' + num) {
          ele.classList.add('active-body');
      } else {
          ele.classList.remove('active-body');
      }
  })
}
