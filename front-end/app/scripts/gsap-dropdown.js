$(document).ready(function() {
	$('.dropdown .appended-file a').click(function(e) {
	  $(this).closest('.dropdown').find('.selection-list ul').toggle();
	  $(this).closest('.dropdown').parent().removeClass('icon-icon-arrow-down').addClass('icon-icon-arrow-up');
	  e.preventDefault();
	  e.stopPropagation();
	});

	$('.dropdown .selection-list ul li a').click(function() {
	  var text = $(this).html();
	  var selectedFile = $(this).parent().data('value');
	  $(this).closest('.dropdown').find('.appended-file a span').html(text);
	  $(this).closest('.dropdown').find('.appended-file .selected-file').html(selectedFile);
	  $(this).closest('.dropdown').find('.selection-list ul').hide();
	  $(this).closest('.dropdown').parent().removeClass('icon-icon-arrow-up').addClass('icon-icon-arrow-down');	  
	}); 

	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (! $clicked.parents().hasClass('dropdown')) {
			$('.dropdown .selection-list ul').hide();
		}
		$('.dropdown').parent().removeClass('icon-icon-arrow-up').addClass('icon-icon-arrow-down');
	});
});

function downloadFile(currFile) {
	var selectedVal = $(currFile).parent().parent().find('.appended-file .selected-file').html();
	console.log('selected file::' + selectedVal);
	var link = document.createElement('a');
	document.body.appendChild(link);
	link.href='C:/Users/shikumari/Desktop/GSAP%20folder/GSAP%20Repo/app/fonts/' + selectedVal;
	link.click();
}
