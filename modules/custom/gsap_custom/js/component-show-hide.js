(function ($, Drupal) {
Drupal.behaviors.myBehavior = {
  attach: function (context, settings) {	  
	  // on page load.
    $('select[data-drupal-selector$="field-position"]', context).each(function() {
        var id = escapeAdminHtml($(this).attr('data-drupal-selector'));
        var loc = $("[data-drupal-selector="+id+"]").val();
        if(loc == 'right') {
                var right_id = id.replace("position", "content-wrapper");
              $("[data-drupal-selector="+right_id+"]").hide(); 
        }
        if(loc == 'content') {
                var content_id = id.replace("position", "right-component-wrapper");
              $("[data-drupal-selector="+content_id+"]").hide(); 
        } 
        if(loc == '_none') {
                var content_id = id.replace("position", "right-component-wrapper");
              $("[data-drupal-selector="+content_id+"]").hide(); 

              var right_id = id.replace("position", "content-wrapper");
              $("[data-drupal-selector="+right_id+"]").hide();
        }		 
      });
	  
	  // on change.
	  $('select[data-drupal-selector$="field-position"]').on('change', function() {
		  var id = escapeAdminHtml($(this).attr('data-drupal-selector'));
		  var loc = $("[data-drupal-selector="+id+"]").val();
		  if(loc == 'right') {
        	var right_id = id.replace("position", "right-component-wrapper");
        	var content_id = id.replace("position", "content-wrapper");
        	$("[data-drupal-selector="+right_id+"]").show(); 
        	$("[data-drupal-selector="+content_id+"]").hide(); 
          }
          if(loc == 'content') {
        	var content_id = id.replace("position", "content-wrapper");
        	var right_id = id.replace("position", "right-component-wrapper");
        	$("[data-drupal-selector="+content_id+"]").show(); 
        	$("[data-drupal-selector="+right_id+"]").hide(); 
          }
          if(loc == '_none') {
          	var content_id = id.replace("position", "content-wrapper");
          	var right_id = id.replace("position", "right-component-wrapper");
          	$("[data-drupal-selector="+content_id+"]").hide(); 
          	$("[data-drupal-selector="+right_id+"]").hide(); 
            }
      });
	 
	  // Hide title location by default.
	  $('fieldset[data-drupal-selector$="form-field-title-location"]').each(function() {
		  var title_id = escapeAdminHtml($(this).attr('data-drupal-selector'));
		  $("[data-drupal-selector="+title_id+"]").hide(); 
	  });
	  
	  // On image upload show title location
	  $('img[data-drupal-selector$="field-asset-image-0-preview"]').each(function() {
		  var img_id = escapeAdminHtml($(this).attr('data-drupal-selector'));
		  var length = $("[data-drupal-selector="+img_id+"]").length;
		  var title_location = img_id.replace("field-asset-image-0-preview", "field-title-location");
		  if(length == 1){
			$("[data-drupal-selector="+title_location+"]").show(); 
		  }
	  });
	  
	  // Change description text for URL in quick links.
	  $('div[id*="form-field-link"][id$=description]').each(function() {
		  var desc_id = escapeAdminHtml($(this).attr('id')); 
		  $("[id="+desc_id+"]").html('You can enter an internal path such as /node/add or an external URL such as http://example.com. Enter <front> to link to the front page.'); 
	  });
          // to escape html string
    function escapeAdminHtml(str) {        
        var div = document.createElement('div');
        div.appendChild(document.createTextNode(str));
        return div.innerHTML;
    }	  
  }
}
})(jQuery, Drupal);