<?php
/**
 * @file
 * Contains \Drupal\gsap_custom\Form\ResumeForm.
 */
namespace Drupal\gsap_custom\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
class UploadForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'upload_form';
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
  	$form['file_upload'] = [
  	  '#type' => 'managed_file',
      '#title' => $this->t('Upload File'),
  	  '#prefix' => '<div class = "upload_chosen_file">',
  	  '#suffix' => '</div>',
  	  '#required' => TRUE,		
      '#upload_location' => 'public://files',
  	  '#description' =>	$this->t('Upload file with extensions pdf, doc, docx, xlsx, xls, mp3, mp4, png, gif, jpg, jpeg.'),	
      '#upload_validators' => [
        'file_validate_extensions' => array('pdf doc docx xlsx xls mp3 mp4 png gif jpg jpeg'),
      ]
    ];
  	
  	$form['submit'] = array(
  			'#type' => 'submit',
  			'#prefix' => '<div class = "upload_submit">',
  			'#suffix' => '</div>',
  			'#value' => t('Submit'),
  	);
  	return $form;
  
  }
   
  public function submitForm(array &$form, FormStateInterface $form_state) {
  	$fid = $form_state->getValue(['file_upload', 0]);
  	if (!empty($fid)) {
  		$file = File::load($fid);
  		$file->setPermanent();
  		$file->save();
  	}
  	list($view_id, $display_id) = ['files', 'page_1'];
  	$route_name = "view.$view_id.$display_id";
  	$form_state->setRedirect($route_name);
  }
}