	<?php
/**
 * SAML 2.0 remote IdP metadata for SimpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-idp-remote 
 */

$metadata['http://dttsts.deloitteresources.com/adfs/services/trust'] = array (
  'entityid' => 'http://dttsts.deloitteresources.com/adfs/services/trust',
  'contacts' => 
  array (
    0 => 
    array (
      'contactType' => 'support',
      'givenName' => 'Prod ADFS AME',
      'emailAddress' => 
      array (
        0 => '',
      ),
      'telephoneNumber' => 
      array (
        0 => '',
      ),
    ),
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://dttsts.deloitteresources.com/adfs/ls/',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://dttsts.deloitteresources.com/adfs/ls/',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://dttsts.deloitteresources.com/adfs/ls/',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://dttsts.deloitteresources.com/adfs/ls/',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://dttstsstage.deloitteresources.com/adfs/services/trust/artifactresolution',
      'index' => 0,
    ),
  ),
  'NameIDFormats' => 
  array (
    0 => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    1 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:persistent',
    2 => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => false,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIFkzCCBHugAwIBAgIQWSJCuGFx+yYAAAAAUNwWPTANBgkqhkiG9w0BAQsFADCBujELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAyMDEyIEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEuMCwGA1UEAxMlRW50cnVzdCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEwxSzAeFw0xNzA0MDQxNDUyMDNaFw0yMDA0MDQxNTIyMDFaMIGuMQswCQYDVQQGEwJVUzERMA8GA1UECBMITmV3IFlvcmsxETAPBgNVBAcTCE5ldyBZb3JrMTAwLgYDVQQKEydEZWxvaXR0ZSBUb3VjaGUgVG9obWF0c3UgU2VydmljZXMsIEluYy4xDDAKBgNVBAsTA0RUUzE5MDcGA1UEAxMwRFRUQURGU1Rva2VuU2lnbmluZ0NlcnRTdGFnZS5hdHJhbWUuZGVsb2l0dGUuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwjyRzL+/IWqg7NKocbOJJVPbhsqV9Yj8D/yZaDnMt+tAeJfd/BLgwY1XnUOkPJEOUNR4o5ijqYXDVIFOIYvKmMHgRFroTu3YZaQTB67qrllxD5pm81LB353haf+cezMX7SgV/ySafB+LrT/isExfC9JIuoW5Nlu+0sgNclGYUy1n9beZzll3lxWduPoAodaEfuXdqcebJBnVMIGsyR5IplbhFJ6BAv2JM3fBIAb2xq++ARHwRlYrlkK12Sh69J8d0bHRBGLnnZPIUKdzl8/2rgRvljGyGnhYpS5ycsT5SmX7QQS7OB1a++XDeAQ4EQyVkvTh5anX7fppKLDdpox1PQIDAQABo4IBnTCCAZkwDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMDMGA1UdHwQsMCowKKAmoCSGImh0dHA6Ly9jcmwuZW50cnVzdC5uZXQvbGV2ZWwxay5jcmwwSwYDVR0gBEQwQjA2BgpghkgBhvpsCgEFMCgwJgYIKwYBBQUHAgEWGmh0dHA6Ly93d3cuZW50cnVzdC5uZXQvcnBhMAgGBmeBDAECAjBoBggrBgEFBQcBAQRcMFowIwYIKwYBBQUHMAGGF2h0dHA6Ly9vY3NwLmVudHJ1c3QubmV0MDMGCCsGAQUFBzAChidodHRwOi8vYWlhLmVudHJ1c3QubmV0L2wxay1jaGFpbjI1Ni5jZXIwOwYDVR0RBDQwMoIwRFRUQURGU1Rva2VuU2lnbmluZ0NlcnRTdGFnZS5hdHJhbWUuZGVsb2l0dGUuY29tMB8GA1UdIwQYMBaAFIKicHTdvFM/z3vU981/p2DGCky/MB0GA1UdDgQWBBTmRhJulcRX8yAianFGu5bHE8z9qTAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUAA4IBAQB8TqhEONIxpXIGWlsuM+nuQGf8QnDL7vpodABBEQBqReKSPlNMqfzbJ4KNIFRzh3+OqIaWzF8OkXJBvhotRErVplDCpCV7EE8vqJkTOAmCX0rO/6POkwXWkHSLjKU7l+Yi0irPpxZsN3Pg6yN6eG6bq7F1WGAvC7GRBgbW2nIlAZAGXWwc1Q7S7nRSDiF9hGVcwl3juh5LzripHJk8gzfTjcWA7gn/Nq3J60YRy0a6Ki/hs0v+csSgHusl4frwOjwYBH+pRQvitQumfrM7O/IknktN6QpaiipYp3lTfRxMbjG/FiPPgAEN3OTdhD18YtXsTbb4eiEuTBUCZpfH4Q5n',
    ),
    1 => 
    array (
      'encryption' => false,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIFkzCCBHugAwIBAgIQWSJCuGFx+yYAAAAAUNwWPTANBgkqhkiG9w0BAQsFADCBujELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUVudHJ1c3QsIEluYy4xKDAmBgNVBAsTH1NlZSB3d3cuZW50cnVzdC5uZXQvbGVnYWwtdGVybXMxOTA3BgNVBAsTMChjKSAyMDEyIEVudHJ1c3QsIEluYy4gLSBmb3IgYXV0aG9yaXplZCB1c2Ugb25seTEuMCwGA1UEAxMlRW50cnVzdCBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eSAtIEwxSzAeFw0xNzA0MDQxNDUyMDNaFw0yMDA0MDQxNTIyMDFaMIGuMQswCQYDVQQGEwJVUzERMA8GA1UECBMITmV3IFlvcmsxETAPBgNVBAcTCE5ldyBZb3JrMTAwLgYDVQQKEydEZWxvaXR0ZSBUb3VjaGUgVG9obWF0c3UgU2VydmljZXMsIEluYy4xDDAKBgNVBAsTA0RUUzE5MDcGA1UEAxMwRFRUQURGU1Rva2VuU2lnbmluZ0NlcnRTdGFnZS5hdHJhbWUuZGVsb2l0dGUuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwjyRzL+/IWqg7NKocbOJJVPbhsqV9Yj8D/yZaDnMt+tAeJfd/BLgwY1XnUOkPJEOUNR4o5ijqYXDVIFOIYvKmMHgRFroTu3YZaQTB67qrllxD5pm81LB353haf+cezMX7SgV/ySafB+LrT/isExfC9JIuoW5Nlu+0sgNclGYUy1n9beZzll3lxWduPoAodaEfuXdqcebJBnVMIGsyR5IplbhFJ6BAv2JM3fBIAb2xq++ARHwRlYrlkK12Sh69J8d0bHRBGLnnZPIUKdzl8/2rgRvljGyGnhYpS5ycsT5SmX7QQS7OB1a++XDeAQ4EQyVkvTh5anX7fppKLDdpox1PQIDAQABo4IBnTCCAZkwDgYDVR0PAQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMDMGA1UdHwQsMCowKKAmoCSGImh0dHA6Ly9jcmwuZW50cnVzdC5uZXQvbGV2ZWwxay5jcmwwSwYDVR0gBEQwQjA2BgpghkgBhvpsCgEFMCgwJgYIKwYBBQUHAgEWGmh0dHA6Ly93d3cuZW50cnVzdC5uZXQvcnBhMAgGBmeBDAECAjBoBggrBgEFBQcBAQRcMFowIwYIKwYBBQUHMAGGF2h0dHA6Ly9vY3NwLmVudHJ1c3QubmV0MDMGCCsGAQUFBzAChidodHRwOi8vYWlhLmVudHJ1c3QubmV0L2wxay1jaGFpbjI1Ni5jZXIwOwYDVR0RBDQwMoIwRFRUQURGU1Rva2VuU2lnbmluZ0NlcnRTdGFnZS5hdHJhbWUuZGVsb2l0dGUuY29tMB8GA1UdIwQYMBaAFIKicHTdvFM/z3vU981/p2DGCky/MB0GA1UdDgQWBBTmRhJulcRX8yAianFGu5bHE8z9qTAJBgNVHRMEAjAAMA0GCSqGSIb3DQEBCwUAA4IBAQB8TqhEONIxpXIGWlsuM+nuQGf8QnDL7vpodABBEQBqReKSPlNMqfzbJ4KNIFRzh3+OqIaWzF8OkXJBvhotRErVplDCpCV7EE8vqJkTOAmCX0rO/6POkwXWkHSLjKU7l+Yi0irPpxZsN3Pg6yN6eG6bq7F1WGAvC7GRBgbW2nIlAZAGXWwc1Q7S7nRSDiF9hGVcwl3juh5LzripHJk8gzfTjcWA7gn/Nq3J60YRy0a6Ki/hs0v+csSgHusl4frwOjwYBH+pRQvitQumfrM7O/IknktN6QpaiipYp3lTfRxMbjG/FiPPgAEN3OTdhD18YtXsTbb4eiEuTBUCZpfH4Q5n',
    ),
  ),
);
