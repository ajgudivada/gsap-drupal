<?php

$metadata['__DYNAMIC:1__'] = array(
	'host' => '__DEFAULT__',
	//'privatekey' => 'server.pem',
	//'certificate' => 'server.crt',
	'auth' => 'example-userpass',
	'authproc' => array(
		// Convert LDAP names to WS-Fed Claims.
		100 => array('class' => 'core:AttributeMap', 'name2claim'),
	),
);

$metadata['https://dttstsstage.deloitteresources.com/adfs/ls/'] = array(
    'host' => 'https://dttstsstage.deloitteresources.com/adfs/ls/',
 // X.509 key and certificate. Relative to the cert directory.
  //      'privatekey' => 'server.pem',
    //    'certificate' => 'server.crt',
'certFingerprint' => 'AA66BB9B6A04F53DF99AE1A96C424038B7771941',
    /* Configuration options for the first IdP. */
);
