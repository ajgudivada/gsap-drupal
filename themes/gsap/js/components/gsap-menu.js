//function to add remove class from the menu - select/deselect menu item
function navigateToPage(event) {
    
        var menuItems = document.querySelectorAll('.primary-menu ul li');
        menuItems.forEach(function (ele) {
            //ele.classList.remove('active-menu-item');            
        });
        
        var clickedMenuItem = event.currentTarget.parentElement;
        clickedMenuItem.classList.add('active-menu-item');        
    
}

    
function togglePrimaryMenu(element){
        //var menuItems = document.querySelector('.primary-menu>ul');
	    var menuItems = document.querySelector('.primary-menu ul');
        if(menuItems.classList.contains('menu-open')){
            menuItems.style.left='-100%';
            menuItems.classList.remove('menu-open');
            document.body.style.overflow='visible';
            menuItems.classList.remove('ul-bg-grey');                                        
        }else{
        	menuItems.classList.remove('ul-bg-grey');
            menuItems.style.left='0';
            menuItems.classList.add('menu-open');
            document.body.style.overflow='hidden';
            setTimeout(function(){
            if(!menuItems.classList.contains('ul-bg-grey')) {
               menuItems.classList.add('ul-bg-grey');
            }},500);

        }
}
    