$(document).ready(function() {
	$('.dropdown .appended-file a').click(function(e) {
	  $(this).closest('.dropdown').parent().toggleClass('icon-icon-arrow-down').toggleClass('icon-icon-arrow-up')
	  $(this).closest('.dropdown').find('.selection-list ul').toggle();
	  e.preventDefault();
	  e.stopPropagation();
	});

	$('.dropdown .selection-list ul li a').click(function() {
	  var text = $(this).html();
	  var ogSelectedFile = $(this).parent().data('value');
	  var selectedFile = escapeHtml(ogSelectedFile);
	  var findme = "|";

	  if ( selectedFile.indexOf(findme) > -1 ) {
		   var hrefArr = selectedFile.split("|");
	  } else {
		  var hrefArr = [selectedFile];
	  }
	  //var hrefArr = selectedFile.split("|");
	  var linkArr = $(this).parents(".custom-dropdown-link").children(".download-file").children("a");
	  var index;
	  //assuming linkArr.length and hrefArr.length will be equal always. TODO: add checks
	  for(index in linkArr){
	    linkArr[index].href=hrefArr[index];
            if(hrefArr[index] == '#'){
              linkArr[index].classList.add('hide');
            }else if(linkArr[index].classList){
              linkArr[index].classList.remove('hide');
            }
          }
	  $(this).closest('.dropdown').find('.appended-file a span').html(text);
	  $(this).closest('.dropdown').find('.appended-file .selected-file').html(selectedFile);
	  $(this).closest('.dropdown').find('.selection-list ul').hide();
	  $(this).closest('.dropdown').parent().removeClass('icon-icon-arrow-up').addClass('icon-icon-arrow-down');
	});

	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (! $clicked.parents().hasClass('dropdown')) {
			$('.dropdown .selection-list ul').hide();
		}
		$('.dropdown').parent().removeClass('icon-icon-arrow-up').addClass('icon-icon-arrow-down');
	});

	/* Intrest and Prep changes */
	if (window.location.pathname == '/interest-prep') {
		jQuery('.gsap-accordion h2').css('color', '#FFF');
		jQuery('.gsap-accordion').parent().parent().css('background-color', '#53565a');
		var accordion_icons = ["directors_visit.png", "campus_visits.png", "mentorship.png", "gsap-days.png", "presentations.png","panel-discussions.png",  "spotlight.png"];

		jQuery('.gsap-accordion .accordion-panel a.accord-header').each(function (i, obj) {
			var elementText = jQuery('.gsap-accordion .accordion-panel a.accord-header:eq(' + i + ')').text(); console.log(elementText);
			jQuery(this).html('<img style="width:auto;height:22px;padding-right:2px;" src="' + '/themes/gsap/images/interest-prep/' + accordion_icons[i] + '"> ' + elementText);
		});
	}
});

/**
 * added by Amod
 * displays a popup if the href contains no link
 */

function checkLink(id) {
	var linkText = document.getElementById(id).getAttribute("href");
	if(linkText == "" || linkText == "#" || linkText == "#download-file") {
		var ogInnertext = document.getElementById(id).innerHTML;
		var innerText = escapeHTML(ogInnertext);
		//var result = innertext.replace(/^Download\s/i, " ");
		var text = "Sorry, this school does not have a "+innertext+".";
		$(document.getElementById(id)).attr("href","#download-file");
	}
}

// to escape html string
function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
}

// UNSAFE with unsafe strings; only use on previously-escaped ones!
/*function unescapeHtml(escapedStr) {
    var div = document.createElement('div');
    div.innerHTML = escapedStr;
    var child = div.childNodes[0];
    return child ? child.nodeValue : '';
}*/
