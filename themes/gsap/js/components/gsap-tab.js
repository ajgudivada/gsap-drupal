//function to handle tab click event
function switchTabs(event) {
  //get the number of tab pressed
  var txt = event.currentTarget.dataset.index;
  var num = txt.replace(/[^0-9]/g, '');

  var tabListItems = document.querySelectorAll('.tab-items>li>a');
  //toggle active class from header
  var len = tabListItems.length;
  var ind;
	for(ind=0;ind<len;ind++){
	  if (tabListItems[ind].dataset.index == 'tab-item-' + num) {
		  tabListItems[ind].classList.add('active-tab');
		  if($('#tab-item li')[num-1]){
		  $('#tab-item').animate( { scrollLeft: $('#tab-item li')[num-1].getBoundingClientRect().left-$($('#tab-item li')[num-1]).width()-$('#tab-item').scrollLeft() }, 1000);
		  }
	  } else {
		  tabListItems[ind].classList.remove('active-tab');
	  }
	  
  }

  var tabBodyElements = document.querySelectorAll('.tab-body-container>div');
  //toggle active class body
  len = tabBodyElements.length;
	for(ind=0;ind<len;ind++){
	  if (tabBodyElements[ind].dataset.index == 'tab-body-' + num) {
          tabBodyElements[ind].classList.add('active-body');
      } else {
          tabBodyElements[ind].classList.remove('active-body');
      }
	  
  }
  
  $(".tab-items-dropdown")[0].textContent= event.currentTarget.textContent;
  $(".tab-items").addClass('mobile-hide').removeClass('mobile-show');
  $(".tab-items-dropdown")[0].className="tab-items-dropdown icon-icon-arrow-down" ;
}

$("#tabDropdown").click(function(event){
    var visible = $(".tab-items").is(":visible");
    if(visible){
      $(".tab-items").addClass('mobile-hide').removeClass('mobile-show');
      $(".tab-items-dropdown")[0].className="tab-items-dropdown icon-icon-arrow-down" ;
    }
    else{         
    	$(".tab-items").addClass('mobile-show').removeClass('mobile-hide');
        $(".tab-items-dropdown")[0].className="tab-items-dropdown icon-icon-arrow-up";
                  
    }
})
document.addEventListener('DOMContentLoaded', function () {
    var leftbutton = document.getElementById('left-arrow');
	if(leftbutton){
    leftbutton.onclick = function () {
		/* RD: updated function */
        //document.getElementById('tab-item').scrollLeft -= 50;
		var length = $('#tab-item li').length;
		var index;
			for(index=length-1;index>=0;index--){
			if($($('#tab-item li')[index]).offset().left - $($('#tab-item li')[index]).width() < 0 ){
				$('#tab-item').animate( { scrollLeft: $('#tab-item li')[index].getBoundingClientRect().left-$('#tab-item').scrollLeft()-$($('#tab-item li')[index]).width() }, 1000);
				break;
			}
		}
    };
	}
    var rightbutton = document.getElementById('right-arrow');
	if(rightbutton){
    rightbutton.onclick = function () {
		/* RD: updated function */
        //document.getElementById('tab-item').scrollLeft += 50;
		var length = $('#tab-item li').length;
		var index;
			for(index=0;index<length;index++){
			if($($('#tab-item li')[index]).offset().left + $($('#tab-item li')[index]).width()> $('#tab-item').scrollLeft()+$('#tab-item').width()){
				if($('#tab-item li')[index-1]){
				$('#tab-item').animate( { scrollLeft: $('#tab-item li')[index-1].getBoundingClientRect().left }, 1000);
				break;
				}
			}
		}
    };
	}
}, false);
/* RD: tab changes  */

/*$(window).resize(function(){
if($("#tab-item") && $(".wrapper-tab")){
if($("#tab-item")[0].scrollWidth > $(".wrapper-tab").width()){
	$(".side-arrow").addClass('desk-show').removeClass('desk-hide');
}
else{
	$(".side-arrow").addClass('desk-hide').removeClass('desk-show');
}
}
});
$(document).ready(function(){
    if($("#tab-item") && $(".wrapper-tab")){
if($("#tab-item")[0].scrollWidth > $(".wrapper-tab").width()){
	$(".side-arrow").addClass('desk-show').removeClass('desk-hide');
}
else{
	$(".side-arrow").addClass('desk-hide').removeClass('desk-show');
}
}
});*/

function resizer() {
if($("#tab-item")[0] && $(".wrapper-tab")){
if($("#tab-item")[0].scrollWidth > Math.ceil($("#tab-item").width())){
	$(".side-arrow").addClass('desk-show').removeClass('desk-hide');
}
else{
	$(".side-arrow").addClass('desk-hide').removeClass('desk-show');
}
}
}
$(document).ready(function(){resizer();});
$(window).off("resize", resizer);
$(window).resize(resizer);