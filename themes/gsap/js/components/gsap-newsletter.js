//js to handle select onchange event
$('#newsletter-dropdown').on('change', function() {
   var value=$(this).val();
   $("#downlink").attr("href",value);
 });

$('#podcast-dropdown').on('change', function() {
	   var value=$(this).val();
	   $("#download-podcast").attr("href",value);
	 });


$('#sch-drpdwn').on('change', function() {
	   var value=$(this).val();
	   if(value != " ") {
		   var res = value.split("|");
		   $("#sch-profile").attr("href",res[0]);
		   $("#sch-spotlight").attr("href",res[1]);  
	   }  
	   else {
		   $("#sch-profile").attr("href","#");
		   $("#sch-spotlight").attr("href","#");
	   }
	 });
