$(document).ready(function(){
  var loc = escapeHtml(window.location.href);
  var lastSlash = loc.lastIndexOf("/");
  var mapString = escapeHtml(loc.substring(lastSlash));
  $('a.active-tab').removeClass('active-tab');
  $('div.active-body').removeClass('active-body');
  $('[data-value="'+mapString+'"]').addClass('active-tab');
  $('[data-value="body~'+mapString+'"]').addClass('active-body');
  $(".tab-items-dropdown").html($('[data-value="'+mapString+'"]').html());
})